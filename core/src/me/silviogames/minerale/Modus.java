package me.silviogames.minerale;

import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 17.01.2018.
 */

public enum Modus {

    UNKNOWN_SYSTEM("Kristallsystem lernen"),
    UNKNOWN_PUNKTGRUPPE("Punktgruppe lernen"),
    UNKNOWN_NAME("Namen der Punktgruppe lernen"),
    MINERALS("Minerale bestimmen");

    public final String title;

    Modus(String title) {
        this.title = title;
    }

    public Menu get_menu(Main main, FreeTypeFontGenerator generator) {
        Menu r = null;
        switch (this) {
            case UNKNOWN_SYSTEM:
                r = new Menu_System(main, generator);
                break;
            case UNKNOWN_PUNKTGRUPPE:
                r = new Menu_Punktgruppe(main, generator);
                break;
            case UNKNOWN_NAME:
                r = new Menu_Name(main, generator);
                break;
            case MINERALS:
                r = new Menu_Minerals(main, generator);
                break;
            default:

                break;
        }
        return r;
    }


}
