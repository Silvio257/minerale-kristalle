package me.silviogames.minerale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 27.01.2018.
 */

public abstract class Menu_Stringinput extends Menu
{
    private final int buffer_size;
    private final float time_blink = 0.4f, time_reveal_correct = 1.5f, time_reveal_false = 4f;
    private final int max_bar_size = 40;
    protected BitmapFont main_font, font_correct, font_wrong;
    private char[] string_buffer;
    private int char_index = 0;
    private String render_string = "";
    private boolean blink = false, correct = false, reveal = false;
    private float timer_blink = 0f, timer_reveal = 0f;
    private char[] time_bar = new char[ 20 ];

    public Menu_Stringinput( Main main, FreeTypeFontGenerator generator, int buffer_size, int guess_amount )
    {
        super( main, generator, guess_amount );
        this.buffer_size = buffer_size;
        string_buffer = new char[ buffer_size ];
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter( );
        parameter.size = 20;
        main_font = generator.generateFont( parameter ); // font size 12 pixels

        parameter.color = Color.CORAL;
        font_wrong = generator.generateFont( parameter ); // font size 12 pixels

        parameter.color = Color.OLIVE;
        font_correct = generator.generateFont( parameter ); // font size 12 pixels

        init_next_super( );
    }

    public void init_next_super()
    {
        flush_buffer( );
        correct = false;
        reveal = false;
        timer_reveal = 0f;
        render_string = "";
        init_next( );
    }

    protected abstract void init_next();

    public final void render( SpriteBatch batch )
    {
        if ( !reveal )
        {
            render_hint( batch );

            main_font.draw( batch, render_string + ( blink ? "|" : "" ), 100, 100, 300, -1, true );
        } else
        {
            render_hint( batch );
            render_reveal( batch, correct );

            main_font.draw( batch, new String( time_bar ), 100, 90, 600, -1, true );

            if ( correct )
            {
                font_correct.draw( batch, render_string, 100, 100, 300, -1, true );
            } else
            {
                font_wrong.draw( batch, render_string, 100, 100, 300, -1, true );
            }
        }


    }

    @Override
    public final void update( float delta )
    {
        timer_blink += delta;
        if ( timer_blink > time_blink )
        {
            timer_blink = 0;
            blink = !blink;
        }

        if ( reveal )
        {
            timer_reveal += delta;
            float final_time = 10f;
            if ( correct )
            {
                final_time = time_reveal_correct;
                if ( timer_reveal > time_reveal_correct )
                {
                    init_next_super( );
                }
            } else
            {
                final_time = time_reveal_false;
                if ( timer_reveal > time_reveal_false )
                {
                    init_next_super( );
                }
            }
            float percent = timer_reveal / final_time;
            int fill_index = ( int ) ( percent * 20 );
            for ( int i = 0; i < 20; i++ )
            {
                if ( i < fill_index )
                {
                    time_bar[ i ] = '_';
                } else
                {
                    time_bar[ i ] = ' ';
                }
            }

        }

        if ( Gdx.input.isKeyJustPressed( Input.Keys.ESCAPE ) )
        {
            main.change_menu( new Menu_Main( main, generator ) );
        }
    }

    @Override
    public void dispose()
    {

    }

    @Override
    public final void key_pressed( char character )
    {
        if ( reveal )
        {
            if ( character == 32 )
            {
                init_next_super( );
                return;
            }
        } else
        {
            if ( character == 13 && char_index > 2 )
            {
                String check_string = ( new String( string_buffer ) ).trim( );
                reveal = true;
                if ( compare_strings( check_string.toCharArray( ), get_answer( ), 3 ) )
                {
                    correct = true;
                    increment_correct( get_guess_index( ) );
                } else
                {
                    correct = false;
                    increment_wrong( get_guess_index( ) );
                }
                update_progress_bars( );
                return;
            }
            if ( character == '\b' )
            {
                //delete last character
                if ( char_index == 0 )
                {
                } else
                {
                    char_index--;
                    string_buffer[ char_index ] = ' ';
                }
            } else
            {
                if ( character == ' ' ) character = '_';
                if ( char_index < string_buffer.length )
                {
                    string_buffer[ char_index ] = character;
                    char_index++;
                } else
                {
                    System.out.println( "string_buffer is full" );
                }
            }
        }

        render_string = ( new String( string_buffer ) ).trim( );
    }

    protected abstract void render_hint( SpriteBatch batch );

    protected abstract void render_reveal( SpriteBatch batch, boolean correct );

    private boolean compare_strings( char[] buffer, char[] check, int threshold )
    {
        if ( buffer == null || check == null ) return false;
        int error = 0;
        for ( int i = 0; i < check.length; i++ )
        {
            if ( i >= buffer.length )
            {
                error++;
            } else
            {
                if ( check[ i ] != buffer[ i ] )
                {
                    error++;
                }
            }
        }
        if ( error <= threshold )
        {
            return true;
        } else
        {
            return false;
        }
    }

    protected abstract char[] get_answer(); // subclass should tell what the correct answer is


    private void flush_buffer()
    {
        for ( int i = 0; i < string_buffer.length; i++ )
        {
            string_buffer[ i ] = ' ';
        }
        char_index = 0;

        for ( int i = 0; i < time_bar.length; i++ )
        {
            time_bar[ i ] = '_';
        }
    }
}
