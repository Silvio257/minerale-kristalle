package me.silviogames.minerale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 22.01.2018.
 */

public class Menu_Punktgruppe extends Menu {

    public static BitmapFont main_font, symbol_font, symbol_font_red, symbol_font_green;

    private final String title = "nenne die Punktgruppe";
    private final char[] legal_hm_characters = "12346-m".toCharArray();
    private final float blink_time = 0.6f, reveal_time_wrong = 3f, reveal_time_correct = 0.8f;
    private int punkte = 0;
    private int char_buffer_index = 0; //shows current size of char buffer
    private Punktgruppe guess_group;
    private int group_size = 0, group_progress = 0;
    private Punktgruppe.HM_Symbol[] group_array, group_buffer = new Punktgruppe.HM_Symbol[3];
    private char[] char_buffer = new char[3], test_buffer = new char[3];
    private float timer_blink = 0f, timer_reveal = 0f;
    private boolean bool_blink = false, bool_reveal = false, correct_group = false;


    public Menu_Punktgruppe(Main main, FreeTypeFontGenerator generator) {
        super(main, generator, 32);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        main_font = generator.generateFont(parameter); // font size 12 pixels

        parameter.size = 20;
        symbol_font = generator.generateFont(parameter);
        parameter.color = Color.OLIVE;
        symbol_font_green = generator.generateFont(parameter);
        parameter.color = Color.FIREBRICK;
        symbol_font_red = generator.generateFont(parameter);

        flush_char_buffer();

        init_group();
    }

    @Override
    public void render(SpriteBatch batch) {
        if (guess_group != null) {
            main_font.draw(batch, guess_group.name, 0, 200, 192, 1, true);
            if (bool_reveal) {
                guess_group.draw(batch, main_font, Main.WIDTH / 2, 170);
            }
            int begin_x = (Main.WIDTH / 2) - (group_size * 10);

            if (bool_reveal) {
                for (int i = 0; i < group_buffer.length; i++) {
                    if (group_buffer[i] != null) {
                        group_buffer[i].draw(begin_x + (i * 20), 60, correct_group ? symbol_font_green : symbol_font_red, batch);
                    }
                }
            } else {
                for (int i = 0; i < group_buffer.length; i++) {
                    if (group_buffer[i] != null) {
                        group_buffer[i].draw(begin_x + (i * 20), 60, main_font, batch);
                    }
                }
                if (bool_blink)
                    main_font.draw(batch, "_", begin_x + (group_progress * 20), 57);
            }
        }

//        main_font.draw(batch, "Punkte: " + punkte, 0, 300, 192, 1, true);

        // main_font.draw(batch, "char_buffer: [" + char_buffer[0] + "] [" + char_buffer[1] + "] [" + char_buffer[2] + "]", 0, 160, 192, 1, true);

    }

    @Override
    protected int get_guess_index() {
        return guess_group.ordinal();
    }

    @Override
    public void update(float delta) {
        timer_blink += delta;
        if (timer_blink >= blink_time) {
            timer_blink = 0f;
            bool_blink = !bool_blink;
        }

        if (bool_reveal) {
            timer_reveal += delta;
            if (timer_reveal >= (correct_group ? reveal_time_correct : reveal_time_wrong)) {
                init_group();
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            main.change_menu(new Menu_Main(main, generator));
        }
    }

    @Override
    public void dispose() {

    }

    @Override
    public void key_pressed(char character) {

        if (bool_reveal) {
            if (character == 32) {
                init_group();
            }
        } else {
            if (guess_group == null) {
                flush_char_buffer();
            } else {
                System.out.println("char_buffer_index " + char_buffer_index);
                if (character == '\b') {
                    System.out.println("backspace pressed!");
                    //something in buffer
                    if (char_buffer_index < 4) {

                        //delete last char buffer index
                        if (char_buffer_index > 0) {
                            char_buffer[char_buffer_index - 1] = '?';
                            test_buffer[char_buffer_index - 1] = '?';
                            char_buffer_index -= 1;
                            group_buffer[group_progress] = Punktgruppe.HM_Symbol.get_symbol(char_buffer);

                        } else if (char_buffer_index == 0) {
                            //char buffer is empty now.
                            group_buffer[group_progress] = null;

                            if (group_progress > 0) {
                                group_progress -= 1;

                                flush_char_buffer();

                                if (group_buffer[group_progress] != null) {
                                    for (int i = 0; i < 3; i++) {
                                        char_buffer[i] = group_buffer[group_progress].char_seq[i];
                                        test_buffer[i] = group_buffer[group_progress].char_seq[i];
                                    }
                                    group_buffer[group_progress] = Punktgruppe.HM_Symbol.get_symbol(char_buffer);

                                    for (int i = 0; i < 3; i++) {
                                        if (char_buffer[i] == '?') {
                                            break;
                                        } else {
                                            char_buffer_index++;
                                        }
                                    }
                                }
                            }
                        }

                    } else {
                        flush_char_buffer();
                    }
                } else if (character == ' ') {
                    if (group_buffer[group_progress] == null) {
                        if (group_progress == 0) {
                            System.out.println("cannot enter null symbol");
                        } else {
                            check_group();
                        }
                    } else {
                        if (group_buffer[group_progress].partial) {
                            System.out.println("cannot submit partial symbol");
                        } else {
                            check_group();
                        }
                    }
                } else if (character == 13) {
                    if (group_buffer[0] != null) {
                        check_direct();
                    }
                } else {

                    if (char_is_legal(character)) {
                        System.out.println("char is legal");
                        if (char_buffer_index < 3) {
                            //buffer has room for character

                            //first check if new char would create existing hm_symbol

                            test_buffer[char_buffer_index] = character;
                            if (Punktgruppe.HM_Symbol.existing_symbol(test_buffer)) {
                                System.out.println("symbol exists");
                                char_buffer[char_buffer_index] = character;
                                char_buffer_index++;
                                group_buffer[group_progress] = Punktgruppe.HM_Symbol.get_symbol(char_buffer);
                            } else {
                                System.out.println("this symbol does not exists");
                                test_buffer[char_buffer_index] = '?';
                            }
                        }
                    } else {
                        System.out.println("char is illegal");
                    }
                }
            }
        }
    }

    @Override
    public String title() {
        return title;
    }

    private void flush_char_buffer() {
        char_buffer_index = 0;
        char_buffer[0] = '?';
        char_buffer[2] = '?';
        char_buffer[1] = '?';

        test_buffer[0] = '?';
        test_buffer[1] = '?';
        test_buffer[2] = '?';
    }

    private void check_group() {
        flush_char_buffer();
        if (group_progress == 2) {
            //check if all symbols are correct

            boolean error = false;
            for (int i = 0; i < 3; i++) {
                if (i >= group_size) {
                    if (group_buffer[i] != null) {
                        error = true;
                    }
                } else {
                    if (group_array[i] != group_buffer[i]) {
                        error = true;
                    }
                }
            }
            if (!error) {
                punkte++;
                correct_group = true;
                bool_reveal = true;
                increment_correct(guess_group.ordinal());
            } else {
                correct_group = false;
                bool_reveal = true;
                increment_wrong(guess_group.ordinal());
            }
            update_progress_bars();
        } else {
            group_progress++;
        }
    }

    private void check_direct() {
        flush_char_buffer();
        boolean error = false;
        for (int i = 0; i < 3; i++) {
            if (i >= group_size) {
                if (group_buffer[i] != null) {
                    error = true;
                }
            } else {
                if (group_array[i] != group_buffer[i]) {
                    error = true;
                }
            }
        }
        if (!error) {
            punkte++;
            correct_group = true;
            bool_reveal = true;
            increment_correct(guess_group.ordinal());
        } else {
            correct_group = false;
            bool_reveal = true;
            increment_wrong(guess_group.ordinal());
        }
        update_progress_bars();
    }

    private void init_group() {
        guess_group = Punktgruppe.random(chance);

        flush_char_buffer();

        bool_reveal = false;
        bool_blink = true;
        timer_reveal = 0f;
        correct_group = false;

        group_size = 0;
        group_progress = 0;
        if (guess_group.first != null) group_size++;
        if (guess_group.second != null) group_size++;
        if (guess_group.third != null) group_size++;

        group_array = new Punktgruppe.HM_Symbol[group_size];

        group_buffer[0] = null;
        group_buffer[1] = null;
        group_buffer[2] = null;

        int index = 0;
        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                if (guess_group.first != null) {
                    group_array[index] = guess_group.first;
                    index++;
                }
            }
            if (i == 1) {
                if (guess_group.second != null) {
                    group_array[index] = guess_group.second;
                    index++;
                }
            }
            if (i == 2) {
                if (guess_group.third != null) {
                    group_array[index] = guess_group.third;
                    index++;
                }
            }
        }

    }

    private boolean char_is_legal(char character) {
        for (int i = 0; i < legal_hm_characters.length; i++) {
            if (legal_hm_characters[i] == character) {
                return true;
            }
        }
        return false;
    }
}
