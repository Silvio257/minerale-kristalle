package me.silviogames.minerale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 22.01.2018.
 */

public class Menu_Main extends Menu {

    private final String title = "Kristalle & Minerale";
    private final float time_exit = 1f;
    BitmapFont main_font;
    private int menu_index = 0;
    private float timer_exit = 0f;
    private boolean hold_exit = false;
    private char[] exit_bar = new char[20];


    public Menu_Main(Main main, FreeTypeFontGenerator generator) {
        super(main, generator, -1);

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        main_font = generator.generateFont(parameter); // font size 12 pixels

    }


    @Override
    public void render(SpriteBatch batch) {

        for (int i = 0; i < Modus.values().length; i++) {
            if (i == menu_index) {
                main_font.draw(batch, "> " + Modus.values()[i].title + " <", 0, 400 - (20 * i), Main.WIDTH, 1, false);
            } else {
                main_font.draw(batch, Modus.values()[i].title, 0, 400 - (20 * i), Main.WIDTH, 1, false);
            }
        }

        if (hold_exit) {
            main_font.draw(batch, "closing program", 0, 30, Main.WIDTH, 1, false);
            main_font.draw(batch, new String(exit_bar), Main.WIDTH / 4, 26, Main.WIDTH / 2, -1, false);
        }

    }

    @Override
    protected int get_guess_index() {
        return -1;
    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            menu_index++;
            if (menu_index >= Modus.values().length) {
                menu_index = 0;
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            menu_index--;
            if (menu_index < 0) {
                menu_index = Modus.values().length - 1;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            Menu menu = Modus.values()[menu_index].get_menu(main, generator);
            if (menu == null) {
                System.out.println("[ERROR] menu is null");
            } else {
                main.change_menu(menu);
            }
        }

        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            hold_exit = true;
            timer_exit += delta;
            if (timer_exit >= time_exit) {
                Gdx.app.exit();
            }
            float percent = timer_exit / time_exit;
            int fill_index = (int) (percent * 20);
            for (int i = 0; i < 20; i++) {
                if (i < fill_index) {
                    exit_bar[i] = '_';
                } else {
                    exit_bar[i] = ' ';
                }
            }

        } else {
            hold_exit = false;
            timer_exit = 0;

        }
    }

    @Override
    public void dispose() {

    }

    @Override
    public void key_pressed(char character) {

    }

    @Override
    public String title() {
        return title;
    }
}
