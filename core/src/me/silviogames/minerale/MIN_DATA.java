package me.silviogames.minerale;

/**
 * Created by Silvio on 01.02.2018.
 */

public enum MIN_DATA {
    quarz("SiO2", "7", "2,65", "trigonal", "quarz", "1:2_oxide"),
    graphit("C", "1-2", "2-2,5", "hexagonal", "?", "Elemente"),
    schwefel("S", "1,5-2,5", "2", "orthorhombisch", "?", "Elemente"),
    chalcosite("Cu2S", "2,5-3", "5,7", "monoklin", "?", "sulfide"),
    bornite("Cu5FeS4", "3", "5,1", "kubisch", "?", "sulfide"),
    chalcopyrit("CuFeS2", "3,5-4", "4,2", "tetragonal", "?", "sulfide"),
    galena("PbS", "2,5", "7,5", "kubisch", "?", "sulfide"),
    pyrit("FeS2", "6-6,5", "4,5-5,1", "kubisch", "?", "sulfide"),
    markasit("FeS2", "6-6,5", "4,5-5,1", "orthorhombisch", "?", "sulfide"),
    realgar("As4S4", "1,5-2", "3,5", "monoklin", "?", "sulfide"),
    auripigment("As2S3", "1,5-2", "3,5", "monoklin", "?", "sulfide"),
    halit("NaCl", "2-2,5", "2,1", "kubisch", "?", "halide"),
    sylvin("KCl", "2", "2", "kubisch", "?", "halide"),
    fluorit("CaF2", "4", "3,2", "kubisch", "?", "halide"),
    rhodocrosit("MnCO3", "3,5-4", "3,3-3,6", "trigonal", "calcitgruppe", "carbonate"),
    kalzit("CaCO3", "3", "2,6-2,8", "trigonal", "calcitgruppe", "carbonate"),
    aragonit("CaCO3", "3,5", "3", "orthorhombisch", "aragonitgruppe", "carbonate"),
    azurit("CuCO3*Cu(OH)2", "3,5-4", "3,8", "monoklin", "?", "hydroxycarbonate"),
    wulfenit("PbMoO4", "2,5-3", "6,5-7,5", "tetragonal", "?", "sulfide/molybdate"),
    apatit("Ca5(PO4)3/(Cl,F,OH)", "5", "3,2", "hexagonal", "?", "phosphate"),
    vanadinit("Pb5(VO4)3/(Cl)", "3-3,5", "6,8-7,2", "hexagonal", "?", "phosphate"),
    rutil("TiO2", "6-6,5", "4,2", "tetragonal", "?", "oxide/hydoxide"),
    hochquarz("SiO2", "7", "2,65", "hexagonal", "?", "oxide"),
    hamatit("Fe2O3", "5,5-6,5", "5,3", "trigonal", "?", "2:3_oxide"),
    opal("SiO2", "7", "2,65", "amorph", "?", "1:2_oxide"),
    cuprit("Cu2O", "3,5-4", "6", "kubisch", "?", "2:1_oxide"),
    pyrolusit("MnO2", "6", "4,9-5,1", "tetragonal", "?", "1:2_oxide"),
    chromit("FeCr2O4", "5,5-6", "4,1-4,8", "kubisch", "spinellgruppe", "oxide"),
    magnetit("Fe3O4", "5,5", "5", "kubisch", "?", "oxide/hydroxie"),
    korund("Al2O3", "9", "4", "trigonal", "?", "2:3_oxide"),
    spinell("MgAl2O4", "7,5-8", "3,6-4,1", "kubisch", "spinellgruppe", "oxide"),
    pyrolusit_glaskopf("MnO2", "6", "4,5-8", "tetragonal", "?", "1:2_oxide"),
    gips("CaSO4*2H2O", "1,5-2", "2,3", "monoklin", "?", "sulfate"),
    baryt("BaSO4", "3-3,5", "4,3-5", "orthorhombisch", "?", "sulfate"),
    turkis("CuAl6(PO4)2*8H2O", "6", "2,9", "triklin", "?", "phosphate"),
    topas("Al2SiO4(F,OH)2", "8", "3,5", "orthorhombisch", "?", "nesosilikate"),
    olivin("(Mg,Fe)2SiO4", "6,5-7", "3,2-4,3", "orthorhombisch", "?", "nesosilikate"),
    andalusit("Al2SiO5", "7-7,5", "3,2", "orthorhombisch", "AlAlSiO5Gruppe", "nesosilikate"),
    staurolith("Fe2Al9Si4O23(OH)2", "7-7,5", "3,7", "monoklin", "?", "nesosilikate"),
    granat("X3Y2(SiO4)3", "6,5-7", "3,5-4,5", "kubisch", "granatgruppe", "nesosilikate"),
    epidot("Ca2(FeAl2)(SiO4)(Si2O7)OOH", "6-7", "3,3-3,4", "monoklin", "epidotgruppe", "sorosilikate"),
    beryll("Be3Al2(SiO3)6", "7,5-8", "2,6-2,9", "hexagonal", "?", "cyclosilikate"),
    schorl("NaFe3Al6(SiO3)6(BO3)3(OH)4", "7-7,5", "3-3,3", "trigonal", "turmalingruppe", "cyclosilikate"),
    diopsid("CaMg(SiO3)2", "5,5-6,5", "3,2", "monoklin", "klinopyroxene", "kettensilikate"),
    tremolit("Ca2Mg5(Si4O11)2(OH)2", "5-6", "3", "monoklin", "Ca_klinoamphibole", "amphibole"),
    aktinolith("Ca2Fe5(Si4O11)2(OH)2", "5,5-6,5", "3", "monoklin", "Ca-klinoamphibole", "amphibole"),
    glaukophan("Na2(Mg,Fe)3Al2(SiO11)2(OH)2", "5-6", "3", "monoklin", "Na-amphibole", "amphibole"),
    muskovit("KAl2(Si3Al)O10(OH,F)2", "2-3", "2,7-2,9", "monoklin", "glimmergruppe", "TOT-schichtsilikat"),
    phlogopit("Kmg3(Si,Al)4(O10(OH,F)2", "2-2,5", "3", "monoklin", "glimmergruppe", "TOT-schichtsilikat"),
    biotit("K(Mg,Fe)3(Si,Al)4O10(OH,F)2", "2,5-3", "2,7-3,4", "monoklin", "glimmergruppe", "TOT-schichtsilikate"),
    orthoklas("KAlSi3O8", "6", "2,5", "monoklin", "alkalifeldspäte", "gerüstsilikate");


    MIN_DATA(String formel, String mohs, String dichte, String system, String gruppe, String klasse) {
        this.formel = formel;
        this.mohs = mohs;
        this.dichte = dichte;
        this.system = system;
        this.gruppe = gruppe;
        this.klasse = klasse;
    }

    public final String formel, mohs, dichte, system, klasse, gruppe;
}
