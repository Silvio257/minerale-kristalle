package me.silviogames.minerale;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 22.01.2018.
 */

public abstract class Menu {

    private final boolean show_progress_bar;
    private final int max_bar_size = 40, guess_amount;
    protected FreeTypeFontGenerator generator;
    protected Main main;
    protected int[] chance;
    private int[] guess_correct, guess_wrong, bar_correct, bar_wrong;

    public Menu(Main main, FreeTypeFontGenerator generator, int guess_amount) {
        this.generator = generator;
        this.main = main;
        if (guess_amount == -1) {
            this.guess_amount = 0;
            show_progress_bar = false;
        } else {
            show_progress_bar = false; //never show it
            this.guess_amount = guess_amount;
            guess_correct = new int[guess_amount];
            guess_wrong = new int[guess_amount];
            bar_correct = new int[guess_amount];
            bar_wrong = new int[guess_amount];
            chance = new int[guess_amount];
            for (int i = 0; i < guess_amount; i++) {
                chance[i] = 5;
            }
        }

    }

    public abstract void render(SpriteBatch batch);

    public void render_progress(SpriteBatch batch) {
        if (show_progress_bar) {
            int x_pos = 0, width = 192 / guess_amount;
            for (int i = 0; i < guess_amount; i++) {
                x_pos = (192 / guess_amount) * i;
                batch.setColor(Color.FIREBRICK.r, Color.FIREBRICK.g, Color.FIREBRICK.b, 0.4f);
                batch.draw(Main.pixel, x_pos, 0, width, bar_wrong[i]);
                batch.setColor(Color.OLIVE.r, Color.OLIVE.g, Color.OLIVE.b, 0.4f);
                batch.draw(Main.pixel, x_pos, bar_wrong[i], width, bar_correct[i]);
            }
        }
        batch.setColor(Color.WHITE);
    }

    protected void increment_correct(int index) {
        if (index < 0 || index >= guess_correct.length) {
            System.out.println("correct index out of bounds");
        } else {
            guess_correct[index]++;
            if (chance[index] > 1) {
                chance[index]--;
            }
        }
    }

    protected abstract int get_guess_index(); // get index of current guess

    protected void increment_wrong(int index) {
        if (index < 0 || index >= guess_wrong.length) {
            System.out.println("wrong index out of bounds");
        } else {
            guess_wrong[index]++;
            if (chance[index] < 10) {
                chance[index]++;
            }
        }
    }

    protected void update_progress_bars() {
        if (show_progress_bar) {
            //find highest number
            int highest = 1;
            for (int i = 0; i < guess_amount; i++) {
                if (guess_correct[i] + guess_wrong[i] > highest) {
                    highest = guess_correct[i] + guess_wrong[i];
                }
            }
            //calculate relative bar size for every index
            for (int i = 0; i < guess_amount; i++) {
                bar_correct[i] = (int) ((guess_correct[i] / ((float) highest)) * max_bar_size);
                bar_wrong[i] = (int) ((guess_wrong[i] / ((float) highest)) * max_bar_size);
            }
        }
    }

    public abstract void update(float delta);

    public abstract void dispose();

    public abstract void key_pressed(char character);

    public abstract String title();

}
