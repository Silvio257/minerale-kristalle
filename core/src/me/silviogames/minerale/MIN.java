package me.silviogames.minerale;

import com.badlogic.gdx.math.MathUtils;

/**
 * Created by Silvio on 31.01.2018.
 */

public enum MIN //image enum, some share same data
{
    achat("achat", "quarz", MIN_DATA.quarz), //quarz
    aktinolith("aktinolith", "", MIN_DATA.aktinolith),
    aktinolith2("aktinolith", "", MIN_DATA.aktinolith),
    andalusit("andalusit", "", MIN_DATA.andalusit),
    andalusit2("andalusit", "chiastholith", MIN_DATA.andalusit),//chiastholith
    apatit("apatit", "", MIN_DATA.apatit),
    aragonit("aragonit", "", MIN_DATA.aragonit),
    auripigment("auripigment", "rauschgelb", MIN_DATA.auripigment),
    azurit("azurit", "kupferlasur", MIN_DATA.azurit), //gleiche wie malachit
    baryt("baryt", "schwerspat", MIN_DATA.baryt),
    baryt2("baryt", "schwerspat", MIN_DATA.baryt),
    beryll("beryll", "", MIN_DATA.beryll),
    biotit("biotit", "", MIN_DATA.biotit),
    biotit2("biotit", "", MIN_DATA.biotit),
    bornit("bornit", "buntkupferkies", MIN_DATA.bornite),
    chalcopyrit("chalcopyrit", "kupferkies", MIN_DATA.chalcopyrit),
    chalcosit("chalcosit", "kupferglanz", MIN_DATA.chalcosite),
    chromit("chromit", "chromeisenerz", MIN_DATA.chromit),
    cuprit("cuprit", "rotkupfererz", MIN_DATA.cuprit),
    diopsid("diopsid", "", MIN_DATA.diopsid),
    diopsid2("diopsid", "chromdiopsid", MIN_DATA.diopsid),
    disthen("disthen", "andalusit", MIN_DATA.andalusit), //auch andalusit?
    epidot("epidot", "", MIN_DATA.epidot),
    epidot2("epidot", "", MIN_DATA.epidot),
    fluorit("fluorit", "flussspat", MIN_DATA.fluorit),
    fluorit2("fluorit", "flussspat", MIN_DATA.fluorit),
    fluorit3("fluorit", "flussspat", MIN_DATA.fluorit),
    fluorit4("fluorit", "flussspat", MIN_DATA.fluorit),
    galena("galena", "bleiglanz", MIN_DATA.galena),
    galena2("galena", "bleiglanz", MIN_DATA.galena),
    gips("gips", "fasergips", MIN_DATA.gips),
    gips2("gips", "marienglas", MIN_DATA.gips),
    glaukophan("glaukophan", "", MIN_DATA.glaukophan),
    granat("pyrop", "granat", MIN_DATA.granat), //pyrop
    granat2("almandin", "granat", MIN_DATA.granat), //almandin
    granat3("grossular", "granat", MIN_DATA.granat), //grossular
    granat4("melanit", "granat", MIN_DATA.granat), //melanit
    granat5("almandin", "granat", MIN_DATA.granat), //almandin
    granat6("uwarowit", "granat", MIN_DATA.granat), //uwarowit
    graphit("graphit", "", MIN_DATA.graphit),
    halit("halit", "steinsalz", MIN_DATA.halit),
    hamatit("hämatit", "roteisenstein", MIN_DATA.hamatit), //roteisenstein
    hamatit2("hämatit", "", MIN_DATA.hamatit),
    hamatit3("hämatit", "roter_glaskopf", MIN_DATA.hamatit), //roter glaskopf
    jaspis("jaspis", "quarz", MIN_DATA.quarz),
    kalzit("kalzit", "kalkspat", MIN_DATA.kalzit),
    korund("korund", "", MIN_DATA.korund),
    korund2("korund", "", MIN_DATA.korund),
    korund3("korund", "", MIN_DATA.korund),
    magnetit("magnetit", "magneteisen", MIN_DATA.magnetit),
    magnetit2("magnetit", "", MIN_DATA.magnetit),
    malachit("malachit", "", MIN_DATA.azurit), //gleiche wie azurit
    markasit("markasit", "pyrit", MIN_DATA.markasit), //pyrit
    muskovit("muskovit", "", MIN_DATA.muskovit),
    olivin("olivin", "", MIN_DATA.olivin),
    opal("opal", "", MIN_DATA.opal),
    orthoklas("orthoklas", "", MIN_DATA.orthoklas),
    orthoklas2("orthoklas", "", MIN_DATA.orthoklas),
    orthoklas3("orthoklas", "", MIN_DATA.orthoklas),
    phlogopit("phlogopit", "biotit", MIN_DATA.phlogopit), //biotit
    pyrit("pyrit", "schwefeleisen", MIN_DATA.pyrolusit),
    pyrit2("pyrit", "schwefeleisen", MIN_DATA.pyrit),
    pyrolusit("pyrolusit", "braunstein", MIN_DATA.pyrolusit),
    pyrolusit2("pyrolusit", "schwarzer_glaskopf", MIN_DATA.pyrolusit_glaskopf),
    quarz("rauchquarz", "quarz", MIN_DATA.quarz), //rauchquarz
    quarz2("flint", "quarz", MIN_DATA.quarz), //flint feuerstein
    quarz3("hochquarz", "quarz", MIN_DATA.hochquarz), //hochquarz
    quarz4("milchquarz", "quarz", MIN_DATA.quarz), //milchquarz
    quarz5("bergkristall", "quarz", MIN_DATA.quarz), //bergkristall
    quarz6("amethyst", "quarz", MIN_DATA.quarz), //amethyst
    quarz7("rosenquarz", "quarz", MIN_DATA.quarz), //rosenquarz
    quarz8("chalcedon", "quarz", MIN_DATA.quarz), //chalcedon
    realgar("realgar", "rauschrot", MIN_DATA.realgar),
    rhodocrosit("rhodocrosit", "himbeerspat", MIN_DATA.rhodocrosit),
    rutil("rutil", "", MIN_DATA.rutil),
    schorl("schörl", "", MIN_DATA.schorl),  //turmalin
    schwefel("schwefel", "", MIN_DATA.schwefel),
    spinell("spinell", "", MIN_DATA.spinell),
    staurolith("strauolith", "", MIN_DATA.staurolith),
    staurolith2("staurolith", "", MIN_DATA.staurolith),
    sylvin("sylvin", "", MIN_DATA.sylvin),
    topas("topas", "", MIN_DATA.topas),
    tremolit("tremolit", "", MIN_DATA.tremolit),
    turkis("türkis", "", MIN_DATA.turkis),
    vanadinit("vanadinit", "", MIN_DATA.vanadinit),
    wulfenit("wulfenit", "gelbbleierz", MIN_DATA.wulfenit);

    public final String first_name, second_name;
    public final MIN_DATA data;

    public static MIN random() {
        return values()[MathUtils.random(0, values().length - 1)];
    }

    MIN(String first_name, String second_name, MIN_DATA data) {
        this.first_name = first_name;
        this.second_name = second_name;
        this.data = data;
    }

}
