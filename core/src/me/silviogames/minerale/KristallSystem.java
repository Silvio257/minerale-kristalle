package me.silviogames.minerale;

/**
 * Created by Silvio on 17.01.2018.
 */

public enum KristallSystem {
    TRIKLIN(),
    MONOKLIN(),
    ORTHORHOMBISCH(),
    TETRAGONAL(),
    TRIGONAL(),
    HEXAGONAL(),
    KUBISCH();

    public final String text;

    KristallSystem(){
        this.text = name().toLowerCase();
    }



}
