package me.silviogames.minerale;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Silvio on 17.01.2018.
 */

public enum Punktgruppe {

    TRIKLIN_PEDIAL(KristallSystem.TRIKLIN, "triklin pedial", "C", "1", null, HM_Symbol.NUM_1, null),
    TRIKLIN_PINAKOIDAL(KristallSystem.TRIKLIN, "triklin pinakoidal", "C", "i", null, HM_Symbol.NUM_1_INVERS, null),

    MONOKLIN_SPHENOIDISCH(KristallSystem.MONOKLIN, "monoklin sphenoidisch", "C", "2", null, HM_Symbol.NUM_2, null),
    MONOKLIN_DOMATISCH(KristallSystem.MONOKLIN, "monoklin domatisch", "C", "s", null, HM_Symbol.MIRROR, null),
    MONOKLIN_PRISMATISCH(KristallSystem.MONOKLIN, "monoklin prismatisch", "C", "2h", null, HM_Symbol.COMB_2_OVER_M, null),

    ORTHORHOMBISCH_DISPHENOIDISCH(KristallSystem.ORTHORHOMBISCH, "rhombisch_disphenoidisch", "D", "2", HM_Symbol.NUM_2, HM_Symbol.NUM_2, HM_Symbol.NUM_2),
    ORTHORHOMBISCH_PYRAMIDAL(KristallSystem.ORTHORHOMBISCH, "rhombisch_pyramidal", "C", "2v", HM_Symbol.MIRROR, HM_Symbol.MIRROR, HM_Symbol.NUM_2),
    ORTHORHOMBISCH_DIPYRAMIDAL(KristallSystem.ORTHORHOMBISCH, "rhombisch_dipyramidal", "D", "2h", HM_Symbol.MIRROR, HM_Symbol.MIRROR, HM_Symbol.MIRROR),

    TETRAGONAL_PYRAMIDAL(KristallSystem.TETRAGONAL, "tetragonal_pyramidal", "C", "4", null, HM_Symbol.NUM_4, null),
    TETRAGONAL_DISPHENOIDISCH(KristallSystem.TETRAGONAL, "tetragonal_disphenoidisch", "S", "4", null, HM_Symbol.NUM_4_INVERS, null),
    TETRAGONAL_DIPYRAMIDISCH(KristallSystem.TETRAGONAL, "tetragonal_dipyramidal", "C", "4h", null, HM_Symbol.COMB_4_OVER_M, null),
    TETRAGONAL_TRAPEZOEDRISCH(KristallSystem.TETRAGONAL, "tetragonal_trapezoedrisch", "D", "4", HM_Symbol.NUM_4, HM_Symbol.NUM_2, HM_Symbol.NUM_2),
    DITETRAGONAL_PYRAMIDAL(KristallSystem.TETRAGONAL, "ditetragonal_pyramidal", "C", "4v", HM_Symbol.NUM_4, HM_Symbol.MIRROR, HM_Symbol.MIRROR),
    TETRAGONAL_SKALENOEDRISCH(KristallSystem.TETRAGONAL, "tetragonal_skalenoedrisch", "D", "2d", HM_Symbol.NUM_4_INVERS, HM_Symbol.NUM_2, HM_Symbol.MIRROR),
    DITETRAGONAL_DIPYRAMIDAL(KristallSystem.TETRAGONAL, "ditetragonal_dipyramidal", "D", "4h", HM_Symbol.COMB_4_OVER_M, HM_Symbol.MIRROR, HM_Symbol.MIRROR),

    TRIGONAL_PYRAMIDAL(KristallSystem.TRIGONAL, "trigonal_pyramidal", "C", "3", null, HM_Symbol.NUM_3, null),
    TRIGONAL_RHOMBOEDRISCH(KristallSystem.TRIGONAL, "rhomboedrisch", "C", "3i", null, HM_Symbol.NUM_3_INVERS, null),
    TRIGONAL_TRAPEZOEDRISCH(KristallSystem.TRIGONAL, "trigonal_trapezoedrisch", "D", "3", HM_Symbol.NUM_3, HM_Symbol.NUM_2, null),
    DITRIGONAL_PYRAMIDAL(KristallSystem.TRIGONAL, "ditrigonal_pyramidal", "C", "3v", HM_Symbol.NUM_3, HM_Symbol.MIRROR, null),
    DITRIGONAL_SKALENOEDRISCH(KristallSystem.TRIGONAL, "ditrigonal_skalenoedrisch", "D", "3d", HM_Symbol.NUM_3_INVERS, HM_Symbol.MIRROR, null),

    HEXAGONAL_PYRAMIDAL(KristallSystem.HEXAGONAL, "hexagonal_pyramidal", "C", "6", null, HM_Symbol.NUM_6, null),
    TRIGONAL_DIPYRAMIDAL(KristallSystem.HEXAGONAL, "trigonal_dipyramidal", "C", "3h", null, HM_Symbol.NUM_6_INVERS, null),
    HEXAGONAL_DIPYRAMIDAL(KristallSystem.HEXAGONAL, "hexagonal_dipyramidal", "C", "6h", null, HM_Symbol.COMB_6_OVER_M, null),
    HEXAGONAL_TRAPEZOEDRISCH(KristallSystem.HEXAGONAL, "hexagonal_trapezoedrisch", "D", "6", HM_Symbol.NUM_6, HM_Symbol.NUM_2, HM_Symbol.NUM_2),
    DIHEXAGONAL_PYRAMIDAL(KristallSystem.HEXAGONAL, "dihexagonal_pyramidal", "C", "6v", HM_Symbol.NUM_6, HM_Symbol.MIRROR, HM_Symbol.MIRROR),
    DITRIGONAL_DIPYRAMIDAL(KristallSystem.HEXAGONAL, "ditrigonal_dipyramidal", "D", "3h", HM_Symbol.NUM_6_INVERS, HM_Symbol.MIRROR, HM_Symbol.NUM_2),
    DIHEXAGONAL_DIPYRAMIDAL(KristallSystem.HEXAGONAL, "dihexagonal_dipyramidal", "D", "6h", HM_Symbol.COMB_6_OVER_M, HM_Symbol.MIRROR, HM_Symbol.MIRROR),

    TETRAEDRISCH_PENTAGONDODEKAEDRISCH(KristallSystem.KUBISCH, "tetraedrisch_pentagondodekaedrisch", "T", "", HM_Symbol.NUM_2, HM_Symbol.NUM_3, null),
    DISDODEKAEDRISCH(KristallSystem.KUBISCH, "disdodekaedrisch", "T", "h", HM_Symbol.MIRROR, HM_Symbol.NUM_3_INVERS, null),
    PENTAGON_IKOSITETRAEDRISCH(KristallSystem.KUBISCH, "pentagon_ikositetraedrisch", "O", "", HM_Symbol.NUM_4, HM_Symbol.NUM_3, HM_Symbol.NUM_2),
    HEXAKISTETRAEDRISCH(KristallSystem.KUBISCH, "hexakistetraedrisch", "T", "d", HM_Symbol.NUM_4_INVERS, HM_Symbol.NUM_3, HM_Symbol.MIRROR),
    HEXAKISOKTAEDRISCH(KristallSystem.KUBISCH, "hexakisoktaedrisch", "O", "h", HM_Symbol.MIRROR, HM_Symbol.NUM_3_INVERS, HM_Symbol.MIRROR);

    public final KristallSystem system;
    public final String name, schonflies, schonflies_index;
    public final HM_Symbol first, second, third;

    Punktgruppe(KristallSystem system, String name, String schonflies, String schonflies_index, HM_Symbol first, HM_Symbol second, HM_Symbol third) {
        this.system = system;
        this.name = name;
        this.schonflies = schonflies;
        this.schonflies_index = schonflies_index;
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public static Punktgruppe random() {
        return values()[MathUtils.random(0, values().length - 1)];
    }

    public static Punktgruppe random(int[] chance) {
        if (chance == null || chance.length != 32) {
            return random();
        } else {
            Array<Punktgruppe> chance_list = new Array<Punktgruppe>();
            for (int i = 0; i < values().length; i++) {
                for (int j = 0; j < chance[i]; j++) {
                    chance_list.add(values()[i]);
                }
            }
            return chance_list.random();
        }
    }

    public void draw(SpriteBatch batch, BitmapFont font, int pos_x, int pos_y) {
        if (first != null) {
            first.draw(pos_x - 20, pos_y, font, batch);
        }
        if (second != null) {
            second.draw(pos_x, pos_y, font, batch);
        }
        if (third != null) {
            third.draw(pos_x + 20, pos_y, font, batch);
        }
    }


    public enum HM_Symbol {
        NONE("-1", "999", true), //none
        MIRROR("m", "m??", false), // m
        NUM_1("1", "1??", false), // 1
        NUM_1_INVERS("1", "-1?", false), // -1
        INVERS("-", "-??", true),
        NUM_2("2", "2??", false), //2
        NUM_3("3", "3??", false), // 3
        NUM_3_INVERS("3", "-3?", false), //-3
        NUM_4("4", "4??", false), //4
        NUM_4_INVERS("4", "-4?", false), //-4
        NUM_6("6", "6??", false), //6
        NUM_6_INVERS("6", "-6?", false), //-6
        COMB_2_OVER_M("2", "2-m", false), // 2 - m
        COMB_4_OVER_M("4", "4-m", false), // 2 - 4
        COMB_6_OVER_M("6", "6-m", false),
        COMB_2("2", "2-?", true),
        COMB_4("4", "4-?", true),
        COMB_6("6", "6-?", true),; // 2 - 6

        public final String num;
        public final char[] char_seq;
        public final boolean partial;

        HM_Symbol(String num, String char_seq, boolean partial) {

            this.partial = partial;
            this.num = num;
            this.char_seq = char_seq.toCharArray();
        }

        public static boolean existing_symbol(char[] test_char) {
            boolean passt = true;
            for (int i = 0; i < values().length; i++) {
                for (int j = 0; j < 3; j++) {
                    if (values()[i].char_seq[j] != test_char[j]) {
                        passt = false;
                    }
                }
                if (passt) {
                    return true;
                } else {
                    passt = true;
                }
            }
            return false;
        }

        public static HM_Symbol get_symbol(char[] seq) {
            boolean passt = true;
            for (int i = 0; i < values().length; i++) {
                for (int j = 0; j < 3; j++) {
                    if (values()[i].char_seq[j] != seq[j]) {
                        passt = false;
                    }
                }
                if (passt) {
                    return values()[i];
                } else {
                    passt = true;
                }
            }
            return null;
        }

        public boolean correct_symbol(char[] check_seq) {
            for (int i = 0; i < 3; i++) {
                if (char_seq[i] != check_seq[i]) {
                    return false;
                }
            }
            return true;
        }

        public void draw(int pos_x, int pos_y, BitmapFont font, SpriteBatch batch) {
            switch (this) {
                case NONE:
                    font.draw(batch, "ERROR", pos_x, pos_y);
                    break;
                case MIRROR:
                case NUM_1:
                case NUM_2:
                case NUM_3:
                case NUM_4:
                case NUM_6:
                    font.draw(batch, num, pos_x, pos_y);
                    break;
                case NUM_1_INVERS:
                case NUM_3_INVERS:
                case NUM_4_INVERS:
                case NUM_6_INVERS:
                    font.draw(batch, "_", pos_x, pos_y + (font.getLineHeight()));
                    font.draw(batch, num, pos_x, pos_y);
                    break;
                case COMB_2_OVER_M:
                case COMB_4_OVER_M:
                case COMB_6_OVER_M:
                    font.draw(batch, "m", pos_x, pos_y);
                    font.draw(batch, "_", pos_x, pos_y + (font.getLineHeight()));
                    font.draw(batch, num, pos_x, pos_y + (font.getLineHeight() * 1.2f));
                    break;
                case INVERS:
                    font.draw(batch, "_", pos_x, pos_y + (font.getLineHeight()));
                    break;
                case COMB_2:
                case COMB_4:
                case COMB_6:
                    //                    font.draw(batch, "m", pos_x, pos_y);
                    font.draw(batch, "_", pos_x, pos_y + (font.getLineHeight()));
                    font.draw(batch, num, pos_x, pos_y + (font.getLineHeight() * 1.2f));
                    break;
            }
        }
    }


}
