package me.silviogames.minerale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 01.02.2018.
 */

public class Menu_Minerals extends Menu {


    private MIN mineral;
    private BitmapFont font, font_answer;
    private StringInput[] inputs = new StringInput[Guess.values().length];
    private int guess_index = 0;
    private boolean done = false;

    private String[] answers = new String[Guess.values().length];


    public Menu_Minerals(Main main, FreeTypeFontGenerator generator) {
        super(main, generator, -1);

        init_mineral();
        reset_inputs();

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        font = generator.generateFont(parameter);

        parameter.size = 14;
        font_answer = generator.generateFont(parameter);

        StringInput.init_fonts(generator);

    }

    @Override
    public void render(SpriteBatch batch) {
        TextureRegion tr = Res.min_map.get(mineral);
        batch.draw(Res.min_map.get(mineral), 300 - (tr.getRegionWidth() / 2), 600 - tr.getRegionHeight());

        for (int i = 0; i < inputs.length; i++) {
            inputs[i].render(batch);
        }

        for (int i = 0; i < Guess.values().length; i++) {
            font.draw(batch, Guess.values()[i].text, 10, 200 - (30 * i), 100, -1, true);
            if (i < guess_index || done) {
                font_answer.draw(batch, answers[i], 352, 200 - (30 * i), 250, -1, true);
            }
        }

        batch.setColor(1, 1, 1, 0.2f);
        for (int i = 0; i < Guess.values().length; i++) {
            batch.draw(Res.pixel, 0, 205 - (30 * i), 600, 1);
        }
        batch.draw(Res.pixel, 100, 0, 1, 205);
        batch.draw(Res.pixel, 350, 0, 1, 205);

        batch.setColor(Color.WHITE);
    }

    @Override
    protected int get_guess_index() {
        return 0;
    }

    @Override
    public void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            main.change_menu(new Menu_Main(main, generator));
            return;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            if (done) {
                init_mineral();
                reset_inputs();
                guess_index = 0;
                done = false;
            } else {
                Guess g = Guess.values()[guess_index];
                boolean correct = false;
                switch (g) {
                    case NAME:
                        correct = inputs[guess_index].check(mineral.first_name, 2);
                        if (!correct && !mineral.second_name.equals("")) {
                            correct = inputs[guess_index].check(mineral.second_name, 2);
                        }
                        break;
                    case CHEM:
                        correct = inputs[guess_index].check(mineral.data.formel, 0);
                        break;
                    case MOHS:
                        correct = inputs[guess_index].check(mineral.data.mohs, 0);
                        break;
                    case DICHTE:
                        correct = inputs[guess_index].check(mineral.data.dichte, 0);
                        break;
                    case SYSTEM:
                        correct = inputs[guess_index].check(mineral.data.system, 2);
                        break;
                    case GRUPPE:
                        if (mineral.data.gruppe.equals("?")) {
                            if (inputs[guess_index].size() < 2) {
                                correct = true;
                            }
                        } else {
                            correct = inputs[guess_index].check(mineral.data.gruppe, 2);
                        }
                        break;
                    case KLASSE:
                        correct = inputs[guess_index].check(mineral.data.klasse, 2);
                        break;
                }
                if (correct) {
                    //System.out.println("correct " + g.text);
                    inputs[guess_index].correct();
                } else {
                    //System.out.println("wrong " + g.text);
                    inputs[guess_index].wrong();
                }
                if (guess_index < Guess.values().length - 1) {
                    guess_index++;
                } else {
                    done = true;
                }
            }
        }

        if (!done) inputs[guess_index].update(delta);
    }

    private void reset_inputs() {
        for (int i = 0; i < inputs.length; i++) {
            inputs[i] = new StringInput(40, 103, 195 - (30 * i));
        }
    }

    @Override
    public void dispose() {

    }

    @Override
    public void key_pressed(char character) {
        if (!done) {
            inputs[guess_index].keypressed(character);
        }
    }

    private void init_mineral() {
        mineral = MIN.random();
        if (mineral.second_name.equals("")) {
            answers[0] = mineral.first_name;
        } else {
            answers[0] = mineral.first_name + ", " + mineral.second_name;
        }
        answers[1] = mineral.data.formel;
        answers[2] = mineral.data.mohs;
        answers[3] = mineral.data.dichte;
        answers[4] = mineral.data.system;
        answers[5] = mineral.data.gruppe;
        answers[6] = mineral.data.klasse;
    }

    @Override
    public String title() {
        return "bestimme das Mineral";
    }

    private enum Guess {
        NAME("Name", 3), CHEM("Formel", 0), MOHS("Härte", 0), DICHTE("Dichte", 0), SYSTEM("System", 2), GRUPPE("Gruppe", 2), KLASSE("Klasse", 2);

        Guess(String name, int threshold) {
            this.text = name;
            this.threshold = threshold;
        }

        public final String text;
        public final int threshold;
    }
}
