package me.silviogames.minerale;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Main extends ApplicationAdapter implements InputProcessor {
    public static final int WIDTH = 600, HEIGHT = 600;
    public static TextureRegion pixel;
    OrthographicCamera camera;
    SpriteBatch batch;
    Viewport viewport;
    private Menu menu;
    private FreeTypeFontGenerator generator;
    private BitmapFont font_title;
    private Texture sheet_draw;

    @Override
    public void create() {
        batch = new SpriteBatch();
        generator = new FreeTypeFontGenerator(Gdx.files.internal("pixel_font.ttf"));

        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 20;
        font_title = generator.generateFont(parameter);

        camera = new OrthographicCamera();
        viewport = new FitViewport(WIDTH, HEIGHT, camera);

        Gdx.input.setInputProcessor(this);

        this.menu = new Menu_Main(this, generator);

        sheet_draw = new Texture(Gdx.files.internal("pixel.png"));
        pixel = new TextureRegion(sheet_draw, 1, 1, 1, 1);

        Res.load();

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);
    }

    @Override
    public void render() {

        update(Gdx.graphics.getDeltaTime());

        Gdx.gl.glClearColor(0.3f, 0.35f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        if (menu != null) {
            font_title.draw(batch, menu.title(), 0, 550, Main.WIDTH, 1, true);
            menu.render(batch);
            menu.render_progress(batch);
        }

        batch.end();
    }

    @Override
    public void dispose() {
        if (menu != null) menu.dispose();
        batch.dispose();
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
        sheet_draw.dispose();
        Res.dispose();
    }

    private void update(float delta) {


        if (menu != null) {
            menu.update(delta);
        }

    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        if (menu != null) {
            menu.key_pressed(character);
        }
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public void change_menu(Menu menu) {
        if (this.menu != null) {
            this.menu.dispose();
        }
        this.menu = menu;
    }
}
