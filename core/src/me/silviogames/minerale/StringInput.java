package me.silviogames.minerale;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 01.02.2018.
 */

public class StringInput { //add this in class to get string input

    private final int buffer_size;
    private int x, y, char_index;
    private boolean centered, blink;
    private static final float time_blink = 0.4f;
    private float timer_blink = 0f;
    private char[] string_buffer;
    private String string;
    private Mode mode = Mode.guess;
    private static BitmapFont normal, correct, wrong;

    public static void init_fonts(FreeTypeFontGenerator generator) {
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 14;
        normal = generator.generateFont(parameter);
        parameter.color = Color.OLIVE;
        correct = generator.generateFont(parameter);
        parameter.color = Color.SALMON;
        wrong = generator.generateFont(parameter);
    }

    public StringInput(int buffer_size, int pos_x, int pos_y) {
        this.buffer_size = buffer_size;
        this.x = pos_x;
        this.y = pos_y;
        this.centered = centered;
        string_buffer = new char[buffer_size];
        for (int i = 0; i < string_buffer.length; i++) {
            string_buffer[i] = ' ';
        }
        string = new String(string_buffer).trim();
    }

    public void update(float delta) {
        timer_blink += delta;
        if (timer_blink > time_blink) {
            timer_blink = 0;
            blink = !blink;
        }
    }

    public void keypressed(char character) {
        if (mode == Mode.guess) {
            if (character == '\b') {
                //delete last character
                if (char_index == 0) {
                } else {
                    char_index--;
                    string_buffer[char_index] = ' ';
                }
            } else {
                if(character == 0) return;
                if (character == ' ') character = '_';
                if (char_index < string_buffer.length) {
                    string_buffer[char_index] = character;
                    char_index++;
                } else {
                    System.out.println("string_buffer is full");
                }
            }
            string = (new String(string_buffer)).trim();
        }
    }

    public void render(SpriteBatch batch) {
        switch (mode) {
            case correct:
                correct.draw(batch, string, x, y, 250, -1, true);
                break;
            case guess:
                normal.draw(batch, string + (blink ? "|" : ""), x, y, 250, -1, true);
                break;
            case wrong:
                wrong.draw(batch, string, x, y, 250, -1, true);
                break;
        }

    }

    public int size() {
        return char_index;
    } //current buffer size

    public boolean check(String correct, int threshold) {
        char[] correct_buffer = correct.toCharArray();
        char[] check_buffer = string.toCharArray();
        if (correct_buffer == null) return false;
        int error = 0;

        for (int i = 0; i < Math.max(correct_buffer.length, check_buffer.length); i++) {
            if (i >= check_buffer.length || i >= correct_buffer.length) {
                error++;
            } else {
                if (check_buffer[i] != correct_buffer[i]) {
                    error++;
                }
            }
        }
        if (error <= threshold) {
            return true;
        } else {
            return false;
        }
    }

    public enum Mode {
        guess, wrong, correct;
    }

    public void correct() {
        mode = Mode.correct;
    }

    public void wrong() {
        mode = Mode.wrong;
    }


}
