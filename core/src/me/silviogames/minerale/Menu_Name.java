package me.silviogames.minerale;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

/**
 * Created by Silvio on 26.01.2018.
 */

public class Menu_Name extends Menu_Stringinput
{
    private final String title = "nenne den Namen der Kristallklasse";
    private Punktgruppe guess;

    public Menu_Name( Main main, FreeTypeFontGenerator generator )
    {
        super( main, generator, 34, 32 );
    }


    @Override
    protected void init_next()
    {
        guess = Punktgruppe.random( );
    }

    @Override
    protected void render_hint( SpriteBatch batch )
    {
        guess.draw( batch, main_font, Main.WIDTH / 2, 300 );
    }

    @Override
    protected void render_reveal( SpriteBatch batch, boolean correct )
    {
        font_correct.draw( batch, guess.name, 0, 200, 600, 1, true );
    }

    @Override
    protected char[] get_answer()
    {
        if ( guess == null )
        {
            return "null".toCharArray( );
        } else
        {
            return guess.name.toCharArray( );
        }
    }

    @Override
    protected int get_guess_index()
    {
        return guess.ordinal( );
    }

    @Override
    public String title()
    {
        return title;
    }

}
