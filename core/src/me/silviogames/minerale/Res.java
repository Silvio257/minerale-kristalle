package me.silviogames.minerale;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by Silvio on 16.01.2018.
 */

public class Res {

    //load and dispose resources

    public static ObjectMap<MIN, TextureRegion> min_map = new ObjectMap<MIN, TextureRegion>();

    private static TextureAtlas tex;
    private static Texture pixel_raw;
    public static TextureRegion pixel;

    public static void load() {
        pixel_raw = new Texture(Gdx.files.internal("pixel.png"));
        pixel = new TextureRegion(pixel_raw, 1, 1, 1, 1);

        tex = new TextureAtlas("minerale.atlas");

        for (MIN m : MIN.values()) {
            TextureRegion tr = tex.findRegion(m.name());
            if (tr != null) {
                min_map.put(m, tr);
            } else {
                System.out.println("ERROR! region for " + m.name() + " is null");
            }
        }
    }


    public static void dispose() {
        tex.dispose();
        pixel_raw.dispose();
    }

}
