package me.silviogames.minerale.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import me.silviogames.minerale.Main;

public class DesktopLauncher {
    public static void main(String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = (Main.WIDTH);
        config.height = (Main.HEIGHT);
        config.resizable = false;
        config.x = 400;
        config.y = 10;
        config.title = "Kristalle & Minerale";
        new LwjglApplication(new Main(), config);
    }
}
